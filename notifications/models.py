from django.contrib.auth.models import User
from django.db import models
from django.db.models import Q

from notifications.enums import EventType
from notifications.enums import GameStatus
from notifications.utils import get_week_datetime_range


class Team(models.Model):
    name = models.CharField(max_length=100)


class Game(models.Model):
    home_team = models.ForeignKey(
        Team,
        on_delete=models.CASCADE,
        related_name='home_games'
    )
    visitor_team = models.ForeignKey(
        Team,
        on_delete=models.CASCADE,
        related_name='away_games'
    )
    scheduled_for = models.DateTimeField()
    status = models.IntegerField(
        choices=GameStatus.choices(),
        default=GameStatus.PLANNED
    )


class Event(models.Model):
    """
    This model represents a live event that happens during a game.
    It's being created when the app fetches external data from api or scraping.
    This implementation is just a simplified example of what it can look like
    with just type & simple detail field.
    In production application it would be much more complex to represent
    various events & their details that can occur during a game.
    """
    game = models.ForeignKey(
        Game,
        on_delete=models.CASCADE
    )
    type = models.IntegerField(choices=EventType.choices())
    detail = models.TextField(blank=True, null=True)

    def get_live_data(self):
        """
        This method implementation is an example, specifics would depend on
        app specification. It might even be on a different model (Game maybe)
        if multiple events were to be a part of a notification.
        For now I'm assuming - single event, single notification.
        """
        return {
            'game': self.game,
            'type': self.type,
            'detail': self.detail
        }


class NotificationProfile(models.Model):
    user = models.OneToOneField(
        User,
        on_delete=models.CASCADE
    )
    notify_by_email = models.BooleanField(default=False)
    notification_email = models.EmailField()
    notify_by_http = models.BooleanField(default=False)
    notification_url = models.URLField()
    teams = models.ManyToManyField(Team)
    send_weekly = models.BooleanField(default=False)
    send_daily = models.BooleanField(default=False)
    send_live = models.BooleanField(default=False)

    def get_weekly_data(self):
        """
        This method implementation is an example, specifics would depend on
        app specification.
        In final implementation it might even be moved to a class of it's own,
        however for now this solution seems simple enough.
        """
        week_start, week_end = get_week_datetime_range()
        games = self.get_games_for_date_range(week_start, week_end)

        return games

    def get_daily_data(self):
        """
        This method implementation is an example, specifics would depend on
        app specification.
        In final implementation it might even be moved to a class of it's own,
        however for now this solution seems simple enough.
        """
        yesterday, tomorrow = get_week_datetime_range()
        games = self.get_games_for_date_range(yesterday, tomorrow)
        return games

    def get_games_for_date_range(self, beginning, end):
        games = set()
        for team in self.teams.all():
            team_games = list(
                Game.objects.filter(
                    Q(home_team=team) | Q(visitor_team=team)
                ).filter(
                    Q(
                        scheduled_for__gte=beginning
                    ) & Q(
                        scheduled_for__lt=end
                    )
                )
            )
            games.update(team_games)

        games = list(games)
        games.sort(key=lambda game: game.scheduled_for)

        return games