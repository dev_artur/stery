# Generated by Django 3.1.3 on 2020-11-21 11:28

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('notifications', '0003_event_notificationsettings'),
    ]

    operations = [
        migrations.AddField(
            model_name='notificationsettings',
            name='send_daily',
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name='notificationsettings',
            name='send_live',
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name='notificationsettings',
            name='send_weekly',
            field=models.BooleanField(default=False),
        ),
    ]
