from datetime import timedelta

import pytest
from django.contrib.auth.models import User
from django.utils import timezone

from notifications.enums import EventType
from notifications.enums import GameStatus
from notifications.models import Event
from notifications.models import Game
from notifications.models import NotificationProfile
from notifications.models import Team


@pytest.mark.django_db
class TestTeam:

    def test_has_correct_fields(self):
        team = Team.objects.create(name="Team 1")

        assert team.id == 1
        assert team.name == "Team 1"


@pytest.mark.django_db
class TestGame:

    def test_has_correct_fields(self):
        home_team = Team.objects.create(name="Home team")
        visitor = Team.objects.create(name="Visitor")
        game_datetime = timezone.now() + timedelta(days=10)

        game = Game.objects.create(
            home_team=home_team,
            visitor_team=visitor,
            scheduled_for=game_datetime,
            status=GameStatus.PLANNED
        )

        assert game.home_team == home_team
        assert game.visitor_team == visitor
        assert game.scheduled_for == game_datetime
        assert game.status == GameStatus.PLANNED


@pytest.mark.django_db
class TestEvent:

    def setup_method(self):
        self.home_team = Team.objects.create(name="Home team")
        self.visitor = Team.objects.create(name="Visitor")
        self.game = Game.objects.create(
            home_team=self.home_team,
            visitor_team=self.visitor,
            scheduled_for=timezone.now(),
            status=GameStatus.PLAYING
        )

    def test_has_correct_fields(self):
        event = Event.objects.create(
            game=self.game,
            type=EventType.GAME_STARTED,
            detail='The game between Team 1 & Team 2 has started'
        )

        assert event.game == self.game
        assert event.type == EventType.GAME_STARTED
        assert event.detail == 'The game between Team 1 & Team 2 has started'

    def test_get_live_data_returns_correct_data(self):
        event = Event.objects.create(
            game=self.game,
            type=EventType.GAME_STARTED,
            detail='The game between Team 1 & Team 2 has started'
        )

        data = event.get_live_data()

        assert data == {
            'game': self.game,
            'type': EventType.GAME_STARTED,
            'detail': 'The game between Team 1 & Team 2 has started'
        }


@pytest.mark.django_db
class TestNotificationProfile:

    def test_has_correct_fields(self):
        user = User.objects.create()

        profile = NotificationProfile.objects.create(
            user=user,
            notify_by_email=True,
            notification_email='notify@me.com',
            notify_by_http=True,
            notification_url='https://api.notify/me',
            send_weekly=True
        )

        assert profile.user == user
        assert profile.notify_by_email is True
        assert profile.notification_email == 'notify@me.com'
        assert profile.notify_by_http is True
        assert profile.notification_url == 'https://api.notify/me'
        assert profile.send_weekly is True
        assert profile.send_daily is False
        assert profile.send_live is False

    def test_has_teams_as_many_to_many_relation(self):
        user = User.objects.create()
        profile = NotificationProfile.objects.create(user=user)
        teams = [
            Team.objects.create(name=name)
            for name
            in ['Team 1', 'Team 2', 'Team 3']
        ]

        profile.teams.add(*teams[:2])

        assert list(profile.teams.all()) == teams[:2]

    def test_get_weekly_data_returns_games_from_current_week(self):
        teams = [
            Team.objects.create(name=name)
            for name
            in ['Team 1', 'Team 2', 'Team 3']
        ]
        user = User.objects.create()
        profile = NotificationProfile.objects.create(user=user)
        profile.teams.add(*teams[:2])
        today = timezone.now()
        ten_days_ago = today - timedelta(days=10)
        ten_days_ahead = today + timedelta(days=10)
        game_1 = Game.objects.create(
            home_team=teams[0], visitor_team=teams[2], scheduled_for=today
        )
        game_2 = Game.objects.create(
            home_team=teams[2], visitor_team=teams[1], scheduled_for=today
        )
        game_3 = Game.objects.create(
            home_team=teams[2],
            visitor_team=teams[1],
            scheduled_for=ten_days_ago
        )
        game_4 = Game.objects.create(
            home_team=teams[2],
            visitor_team=teams[1],
            scheduled_for=ten_days_ahead
        )

        data = profile.get_weekly_data()

        assert len(data) == 2
        assert game_1 in data
        assert game_2 in data

    def test_get_daily_data_returns_games_from_correct_days(self):
        teams = [
            Team.objects.create(name=name)
            for name
            in ['Team 1', 'Team 2', 'Team 3', 'Team 4']
        ]
        user = User.objects.create()
        profile = NotificationProfile.objects.create(user=user)
        profile.teams.add(*teams[:2])
        today = timezone.now()
        yesterday = today - timedelta(days=1)
        tomorrow = today + timedelta(days=1)
        three_days_ahead = today + timedelta(days=3)
        game_1 = Game.objects.create(
            home_team=teams[0], visitor_team=teams[2], scheduled_for=today
        )
        game_2 = Game.objects.create(
            home_team=teams[2], visitor_team=teams[3], scheduled_for=tomorrow
        )
        game_3 = Game.objects.create(
            home_team=teams[2],
            visitor_team=teams[1],
            scheduled_for=yesterday
        )
        game_4 = Game.objects.create(
            home_team=teams[2],
            visitor_team=teams[1],
            scheduled_for=three_days_ahead
        )

        data = profile.get_daily_data()

        assert len(data) == 2
        assert game_1 in data
        assert game_3 in data