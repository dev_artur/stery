from unittest.mock import patch

from notifications.senders import NotificationSender


@patch('notifications.senders.requests.post', autospec=True)
@patch('notifications.senders.send_mail', autospec=True)
class TestNotificationSender:

    def setup_method(self):

        class NotificationProfileStub:
            def __init__(self):
                self.notify_by_http = False
                self.notify_by_email = False
                self.notification_email = 'user@test.com'
                self.notification_url = 'https://api.notify/me'

        self.notification_profile = NotificationProfileStub()

    def test_sends_both_types_of_notifications(
            self, mock_send_mail, mock_post
    ):
        self.notification_profile.notify_by_http = True
        self.notification_profile.notify_by_email = True
        sender = NotificationSender(self.notification_profile, data={})

        sender.send_notifications()

        assert mock_send_mail.called
        assert mock_post.called

    def test_sends_only_http_notifications(
            self, mock_send_mail, mock_post
    ):
        self.notification_profile.notify_by_http = True
        sender = NotificationSender(self.notification_profile, data={})

        sender.send_notifications()

        assert not mock_send_mail.called
        assert mock_post.called

    def test_sends_only_email_notifications(
            self, mock_send_mail, mock_post
    ):
        self.notification_profile.notify_by_email = True
        sender = NotificationSender(self.notification_profile, data={})

        sender.send_notifications()

        assert mock_send_mail.called
        assert not mock_post.called
