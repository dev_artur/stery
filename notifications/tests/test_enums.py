from notifications.enums import EventType
from notifications.enums import GameStatus


class TestGameStatus:

    def test_enum_has_correct_members(self):
        assert GameStatus.PLANNED == 1
        assert GameStatus.PLAYING == 2
        assert GameStatus.FINISHED == 3

    def test_choices_returns_list_of_member_tuples(self):
        choices = GameStatus.choices()

        assert choices == [
            (member.value, member.name) for member in GameStatus
        ]


class TestEventType:

    def test_enum_has_correct_members(self):
        assert EventType.GAME_STARTED == 1
        assert EventType.GAME_ENDED == 2
        assert EventType.SCORE_CHANGED == 3

    def test_choices_returns_list_of_member_tuples(self):
        choices = EventType.choices()

        assert choices == [
            (member.value, member.name) for member in EventType
        ]