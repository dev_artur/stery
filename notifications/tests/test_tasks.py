from unittest.mock import patch, call

import pytest
from django.contrib.auth.models import User
from django.utils import timezone

from notifications.enums import EventType
from notifications.models import Event
from notifications.models import Game
from notifications.models import NotificationProfile
from notifications.models import Team
from notifications.tasks import send_all_daily_notifications
from notifications.tasks import send_all_live_notifications
from notifications.tasks import send_all_weekly_notifications
from notifications.tasks import send_single_daily_notification
from notifications.tasks import send_single_live_notification
from notifications.tasks import send_single_weekly_notification


@pytest.mark.django_db
class TestSendAllWeeklyNotifications:

    @patch(
        'notifications.tasks.send_single_weekly_notification', autospec=True
    )
    @patch('notifications.tasks.group', autospec=True)
    def test_creates_job_signature_for_correct_profiles(
            self, mock_group, mock_send_notification
    ):
        user_1 = User.objects.create(username='1')
        user_2 = User.objects.create(username='2')
        profile_1 = NotificationProfile.objects.create(
            user=user_1, send_weekly=True
        )
        profile_2 = NotificationProfile.objects.create(
            user=user_2, send_daily=True
        )
        send_all_weekly_notifications()

        calls = mock_send_notification.s.call_args_list
        assert calls == [call(profile_1.id)]
        assert len(calls) == 1


@pytest.mark.django_db
class TestSendSingleWeeklyNotification:

    @patch('notifications.tasks.NotificationSender')
    def test_sends_notification_with_correct_data(self, mock_sender):
        user_1 = User.objects.create(username='1')
        user_2 = User.objects.create(username='2')
        profile_1 = NotificationProfile.objects.create(
            user=user_1, send_weekly=True
        )
        profile_2 = NotificationProfile.objects.create(
            user=user_2, send_daily=True
        )

        send_single_weekly_notification(profile_1.id)

        args, kwargs = mock_sender.call_args

        assert args == (profile_1, [])


@pytest.mark.django_db
class TestSendAllDailyNotifications:

    @patch('notifications.tasks.send_single_daily_notification', autospec=True)
    @patch('notifications.tasks.group', autospec=True)
    def test_creates_job_signature_for_correct_profiles(
            self, mock_group, mock_send_notification
    ):
        user_1 = User.objects.create(username='1')
        user_2 = User.objects.create(username='2')
        profile_1 = NotificationProfile.objects.create(
            user=user_1, send_weekly=True
        )
        profile_2 = NotificationProfile.objects.create(
            user=user_2, send_daily=True
        )
        send_all_daily_notifications()

        calls = mock_send_notification.s.call_args_list
        assert calls == [call(profile_2.id)]
        assert len(calls) == 1


@pytest.mark.django_db
class TestSendSingleDailyNotification:

    @patch('notifications.tasks.NotificationSender')
    def test_sends_notification_with_correct_data(self, mock_sender):
        user_1 = User.objects.create(username='1')
        user_2 = User.objects.create(username='2')
        profile_1 = NotificationProfile.objects.create(
            user=user_1, send_weekly=True
        )
        profile_2 = NotificationProfile.objects.create(
            user=user_2, send_daily=True
        )

        send_single_daily_notification(profile_2.id)

        args, kwargs = mock_sender.call_args

        assert args == (profile_2, [])


@pytest.mark.django_db
class TestSendAllLiveNotifications:

    @patch(
        'notifications.tasks.send_single_live_notification', autospec=True
    )
    @patch('notifications.tasks.group', autospec=True)
    def test_creates_job_signature_for_correct_profiles(
            self, mock_group, mock_send_notification
    ):
        teams = [
            Team.objects.create(name=name)
            for name
            in ['Team 1', 'Team 2', 'Team 3', 'Team 4']
        ]
        profile_1 = NotificationProfile.objects.create(
            user=User.objects.create(username='User 1')
        )
        profile_1.teams.add(*teams[:2])
        profile_2 = NotificationProfile.objects.create(
            user=User.objects.create(username='User 2')
        )
        profile_2.teams.add(*teams[2:])
        game = Game.objects.create(
            home_team=teams[0],
            visitor_team=teams[1],
            scheduled_for=timezone.now()
        )

        event = Event.objects.create(game=game, type=1)

        send_all_live_notifications(event.id)

        calls = mock_send_notification.s.call_args_list
        assert calls == [call(profile_1.id, event.id)]
        assert len(calls) == 1


@pytest.mark.django_db
class TestSendSingleLiveNotification:

    @patch('notifications.tasks.NotificationSender')
    def test_sends_notification_with_correct_data(self, mock_sender):
        teams = [
            Team.objects.create(name=name)
            for name
            in ['Team 1', 'Team 2', 'Team 3', 'Team 4']
        ]
        profile_1 = NotificationProfile.objects.create(
            user=User.objects.create(username='User 1')
        )
        profile_1.teams.add(*teams[:2])
        profile_2 = NotificationProfile.objects.create(
            user=User.objects.create(username='User 2')
        )
        profile_2.teams.add(*teams[2:])
        game_1 = Game.objects.create(
            home_team=teams[0],
            visitor_team=teams[1],
            scheduled_for=timezone.now()
        )
        game_2 = Game.objects.create(
            home_team=teams[0],
            visitor_team=teams[2],
            scheduled_for=timezone.now()
        )

        event_1 = Event.objects.create(game=game_1, type=1, detail='Detail 1')
        event_2 = Event.objects.create(game=game_2, type=1, detail='Detail 2')

        send_single_live_notification(profile_1.id, event_1.id)

        args, kwargs = mock_sender.call_args
        expected_data = {
            'game': game_1,
            'type': EventType.GAME_STARTED,
            'detail': 'Detail 1'
        }

        assert args == (profile_1, expected_data)
