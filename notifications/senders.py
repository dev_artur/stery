import requests
from django.conf import settings
from django.core.mail import send_mail


class NotificationSender:

    def __init__(self, notification_profile, data):
        self.notification_profile = notification_profile
        self.data = data

    def send_notifications(self):
        if self.notification_profile.notify_by_http:
            self.send_http_notification()
        if self.notification_profile.notify_by_email:
            self.send_email_notification()

    def send_http_notification(self):
        requests.post(
            self.notification_profile.notification_url,
            self.get_http_data()
        )

    def send_email_notification(self):
        send_mail(
            subject='Sports notifications',
            message=self.get_email_message(),
            from_email=settings.NOTIFICATIONS_FROM_EMAIL,
            recipient_list=[self.notification_profile.notification_email],
            fail_silently=True
        )

    def get_email_message(self):
        """
        This method would transform self.data into an email message.
        Since this may be done in many ways & the message templates have not
        been provided in the task, I'm assuming implementing this method is
        out of the scope
        """
        pass

    def get_http_data(self):
        """
        This method would transform self.data into specific json data format.
        Since this also may be done in multiple ways, I'm also assuming it's
        out of the scope
        """
        pass