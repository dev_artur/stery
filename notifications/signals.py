from django.db.models.signals import post_save
from django.dispatch import receiver

from notifications.models import Event
from notifications.tasks import send_all_live_notifications


@receiver(post_save, sender=Event)
def post_event_created(sender, instance=None, created=False, **kwargs):
    if created:
        send_all_live_notifications.apply_async(instance.id)
