from datetime import datetime, timedelta

from django.utils import timezone


def get_week_datetime_range():
    now = timezone.now()
    now_midnight = datetime.combine(now, datetime.min.time())
    week_start = now_midnight - timedelta(days=now.weekday())
    week_end = week_start + timedelta(days=7)
    return week_start, week_end


def get_previous_and_next_datetime():
    now = timezone.now()
    now_midnight = datetime.combine(now, datetime.min.time())
    previous_datetime = now_midnight - timedelta(days=1)
    next_datetime = now_midnight + timedelta(days=2)
    return previous_datetime, next_datetime