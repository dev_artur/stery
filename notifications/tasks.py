from celery import group
from celery import shared_task
from django.core.exceptions import ObjectDoesNotExist

from notifications.models import Event
from notifications.models import NotificationProfile
from notifications.senders import NotificationSender
from stery.celery import app


@shared_task
def send_all_weekly_notifications():
    notifications = NotificationProfile.objects.filter(
        send_weekly=True
    ).values_list('id', flat=True)
    job = group(
        [
            send_single_weekly_notification.s(notification_id)
            for notification_id in notifications
        ]
    )
    job.apply_async()


@app.task
def send_single_weekly_notification(notification_id):
    try:
        notification_profile = NotificationProfile.objects.get(
            pk=notification_id
        )
        data = notification_profile.get_weekly_data()
        sender = NotificationSender(notification_profile, data)
        sender.send_notifications()
    except ObjectDoesNotExist:
        # log specific error
        return


@shared_task
def send_all_daily_notifications():
    notifications = NotificationProfile.objects.filter(
        send_daily=True
    ).values_list('id', flat=True)
    job = group(
        [
            send_single_daily_notification.s(notification_id)
            for notification_id in notifications
        ]
    )
    job.apply_async()


@app.task
def send_single_daily_notification(notification_id):
    try:
        notification_profile = NotificationProfile.objects.get(
            pk=notification_id
        )
        data = notification_profile.get_daily_data()
        sender = NotificationSender(notification_profile, data)
        sender.send_notifications()
    except ObjectDoesNotExist:
        # log specific error
        return


@shared_task()
def send_all_live_notifications(event_id):
    try:
        event = Event.objects.select_related(
            'game__home_team', 'game__visitor_team'
        ).get(pk=event_id)
        event_teams = [event.game.home_team, event.game.visitor_team]
        notification_profiles = NotificationProfile.objects.filter(
            teams__in=event_teams
        ).distinct().values_list('id', flat=True)
        job = group(
            [
                send_single_live_notification.s(notification_id, event.id)
                for notification_id in notification_profiles
            ]
        )
        job.apply_async()
    except ObjectDoesNotExist:
        # log specific error
        return


@app.task
def send_single_live_notification(notification_id, event_id):
    try:
        notification_profile = NotificationProfile.objects.get(
            pk=notification_id
        )
        event = Event.objects.get(pk=event_id)
        data = event.get_live_data()
        sender = NotificationSender(notification_profile, data)
        sender.send_notifications()
    except ObjectDoesNotExist:
        # log specific error
        return