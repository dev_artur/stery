from enum import IntEnum


class ChoiceEnum(IntEnum):

    @classmethod
    def choices(cls):
        return [(member.value, member.name) for member in cls]


class GameStatus(ChoiceEnum):
    PLANNED = 1
    PLAYING = 2
    FINISHED = 3


class EventType(ChoiceEnum):
    GAME_STARTED = 1
    GAME_ENDED = 2
    SCORE_CHANGED = 3
