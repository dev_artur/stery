celery==5.0.2
Django==3.1.3
pytest==6.1.2
pytest-django==4.1.0
redis==3.5.3
requests==2.25.0